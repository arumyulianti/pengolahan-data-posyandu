<!-- Brand Logo -->
    <a href="#" class="brand-link">
      <span class="brand-text font-weight-light">Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
     
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Balita
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/balita" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tabel Balita</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/balita/create" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Input Data Balita</p>
                </a>
              </li>
          </ul>

          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
             <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Ibu Hamil
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/ibuhamil" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tabel Ibu Hamil</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/ibuhamil/create" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Input Data Ibu Hamil</p>
                </a>
              </li>
            </ul>

            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
             <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Penimbangan Balita
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/penimbangan_balita" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tabel Timbang Balita</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/penimbangan_balita/create" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Input Data Timbang Balita</p>
                </a>
              </li>
            </ul>

             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
             <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Tambah Darah
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/tambah_darah" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tabel Tambah Darah</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/tambah_darah/create" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Input Data Tambah Darah</p>
                </a>
              </li>
            </ul>

             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
             <a href="/home" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Logout
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
        </ul>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
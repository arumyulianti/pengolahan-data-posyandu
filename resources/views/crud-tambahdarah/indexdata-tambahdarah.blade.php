@extends ('crud.layoutmaster')

@section ('title')
  Tabel Tambah Darah
@endsection

@section ('content')
@if (session('success'))
	<div class="alert alert-success">
		{{ session ('success')}}
	</div>
@endif
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama </th>
      <th>Pemberian Untuk</th>
      <th>Tanggal Pemberian</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($posts as $key => $tambah_darah)
    <tr>
      <td> {{ $key + 1}} </td>
      <td> {{ $tambah_darah -> nama }} </td>
      <td> {{ $tambah_darah -> pemberian_untuk }} </td>
      <td> {{ $tambah_darah -> tanggal_pemberian }} </td>
      <td style="display: flex;"> 
        <a href="/tambah_darah/{{$tambah_darah->id}}" class="btn btn-info btn-sm btn-success"> Detail </a> 
         <a href="/tambah_darah/{{$tambah_darah->id}}/edit" class="btn btn-info btn-sm btn-success btn-warning ml-2"> Edit </a>
          <form action="/tambah_darah/{{$tambah_darah->id}}" method="post">
        @csrf
        @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2"> 
      </td> 

    </tr>
    @endforeach
  

</table>
@endsection




     
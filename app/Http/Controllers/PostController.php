<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Balita;

class PostController extends Controller
{
    //
     public function create(){
    	return view('crud.data-balita');
    }

     
  public function simpan(Request $request){
	$request->validate([
        "nama_anak" => 'required',
        "tempat_lahir" => 'required',
        "tanggal_lahir" => 'required',
        "jenis_kelamin" => 'required',
        "nama_ayah" => 'required',
        "nama_ibu" => 'required',
        "alamat" => 'required',
        "umur" => 'required'
      ]);

    	/* $query= DB::table('balita')->insert([ 
    			"nama_anak" => $request["nama_anak"],
    			"tempat_lahir" => $request["tempat_lahir"],
    			"tanggal_lahir" => $request["tanggal_lahir"],
    			"jenis_kelamin" => $request["jenis_kelamin"],
    			"nama_ayah" => $request["nama_ayah"],
    			"nama_ibu" => $request["nama_ibu"],
    			"alamat" => $request["alamat"],
    			"umur" => $request["umur"]
	]); */

  $balita =  new Balita;
  $balita->nama_anak=$request["nama_anak"];
  $balita->tempat_lahir=$request["tempat_lahir"];
  $balita->tanggal_lahir=$request["tanggal_lahir"];
  $balita->jenis_kelamin=$request["jenis_kelamin"];
  $balita->nama_ayah=$request["nama_ayah"];
  $balita->nama_ibu=$request["nama_ibu"];
  $balita->alamat=$request["alamat"];
  $balita->umur=$request["umur"];
  $balita->save();

	return redirect ('/balita')->with('success','Data Berhasil Disimpan!');
    }

  
    public function index(){
   	//$posts = DB::table('balita')->get();
   	//dd($posts);
    $posts = Balita::all();
   	return view ('crud.indexdata-balita', compact('posts')); 
   }

   public function detail($id){
   	// $post = DB::table('balita')->where('id', $id)->first();
   	//dd($post);
    $post = Balita::find($id);
   	return view ('crud.detail', compact('post')); 
   }

   public function edit($id){
   	$post = DB::table('balita')->where('id', $id)->first();
   	//dd($post);
   	return view ('crud.editdata-balita', compact('post')); 
   }

   public function update($id, Request $request){
	$request->validate([
        "nama_anak" => 'required',
        "tempat_lahir" => 'required',
        "tanggal_lahir" => 'required',
        "jenis_kelamin" => 'required',
        "nama_ayah" => 'required',
        "nama_ibu" => 'required',
        "alamat" => 'required',
        "umur" => 'required'
      ]);

    	/*$query = DB::table('balita')
    						->where('id', $id)
    						->update([ 
    			"nama_anak" => $request["nama_anak"],
    			"tempat_lahir" => $request["tempat_lahir"],
    			"tanggal_lahir" => $request["tanggal_lahir"],
    			"jenis_kelamin" => $request["jenis_kelamin"],
    			"nama_ayah" => $request["nama_ayah"],
    			"nama_ibu" => $request["nama_ibu"],
    			"alamat" => $request["alamat"],
    			"umur" => $request["umur"]
	]);*/
  $update = Balita::where('id', $id)->update([
          "nama_anak" => $request["nama_anak"],
          "tempat_lahir" => $request["tempat_lahir"],
          "tanggal_lahir" => $request["tanggal_lahir"],
          "jenis_kelamin" => $request["jenis_kelamin"],
          "nama_ayah" => $request["nama_ayah"],
          "nama_ibu" => $request["nama_ibu"],
          "alamat" => $request["alamat"],
          "umur" => $request["umur"]
  ]);

	return redirect ('/balita')->with('success','Data Berhasil Diubah!');
    }

    public function destroy($id){
    	//$query = DB::table('balita')->where('id', $id)->delete(); 
    	//dd($query);
      Balita::destroy($id);
    	return redirect('/balita')->with ('success','Berhasil Hapus Data');  
    }

}

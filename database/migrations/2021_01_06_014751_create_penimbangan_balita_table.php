<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenimbanganBalitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penimbangan_balita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_anak',150);
            $table->string('tanggal_penimbangan',50);
            $table->string('berat_badan',50);
            $table->string('vitamin',50);
            $table->string('imunisasi',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penimbangan_balita');
    }
}

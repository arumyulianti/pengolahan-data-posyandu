<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balita', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_anak',150);
            $table->string('tempat_lahir',50);
            $table->string('tanggal_lahir',50);
            $table->string('jenis_kelamin',50);
            $table->string('nama_ayah',150);
            $table->string('nama_ibu',150);
            $table->string('alamat',255);
            $table->string('umur',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balita');
    }
}
